import { Button, Layout, Typography, Avatar, Menu, Breadcrumb } from "antd";
import "./App.css";
import { UserOutlined } from "@ant-design/icons";
import SubMenu from "antd/lib/menu/SubMenu";

const { Header, Footer, Sider, Content } = Layout;
const { Title } = Typography;

function App() {
  return (
    <div className="App">
      <Layout>
        <Header style={{ padding: 10 }}>
          <Avatar style={{ float: "right" }} src="/dp.png" />
          <Title style={{ color: "white" }} level={3}>
            Ayesha Munir
          </Title>
        </Header>
        <Layout>
          <Sider>
            <Menu defaultSelectedKeys={["dashboard"]} mode="inline">
              <Menu.Item key="dashboard">Dashboard</Menu.Item>
              <SubMenu
                title={
                  <span>
                    <span>About us</span>
                  </span>
                }
              >
                <Menu.ItemGroup key="aboutUs" title="About Us">
                  <Menu.Item key="location1">Location 1</Menu.Item>
                  <Menu.Item key="location2">Location 2</Menu.Item>
                </Menu.ItemGroup>
              </SubMenu>
            </Menu>
          </Sider>
          <Layout>
            <Content style={{ padding: "0 50px" }}>
              <Breadcrumb style={{ margin: "16px 0" }}>
                <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
              </Breadcrumb>
              <div className="site-layout-content">Content</div>
            </Content>
            <Footer style={{ textAlign: "center" }}>
              Ant Design Example ©2022 Created by Ayesha
            </Footer>
          </Layout>
        </Layout>
      </Layout>
    </div>
  );
}

export default App;
